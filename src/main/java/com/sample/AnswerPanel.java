package com.sample;

import javax.swing.*;

import org.kie.api.runtime.KieSession;

import javax.imageio.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.*;
import java.io.*;

public class AnswerPanel extends JPanel
{

    public AnswerPanel(String answer)
    {
        GridBagLayout gbl = new GridBagLayout();
        setLayout(gbl);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;

        JLabel answerLabel = new JLabel("You should watch movies directed by " + answer);
        answerLabel.setFont(new Font("DejaVu Serif", Font.BOLD, 32));
        gbl.setConstraints(answerLabel, gbc);
        add(answerLabel);
    }
}

