/*
 * Copyright 2010 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sample;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

public class MovieDirectorMain extends JFrame {

	public KieSession kSession;
	
    public static void main(String[] args) {
        KieServices ks = KieServices.Factory.get();
        KieContainer kc = ks.getKieClasspathContainer();
    	KieSession kSession = kc.newKieSession("ksession-rules");
        MovieDirectorMain movieDirectorMain = new MovieDirectorMain();
        kSession.setGlobal("frame", movieDirectorMain);
        movieDirectorMain.kSession = kSession;
        movieDirectorMain.setSize(1280, 720);
        movieDirectorMain.setResizable(false);
        movieDirectorMain.setLocationRelativeTo(null); // Center in screen
        movieDirectorMain.setVisible( true );
        movieDirectorMain.showStartScreen();
    }
    
    public MovieDirectorMain() {
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    public void showStartScreen()
    {
        setLayout(new BorderLayout());
        StartScreen startScreen = new StartScreen(kSession, this);
        add(startScreen, BorderLayout.CENTER);
    }
    
    public String askQuestion(String question, String[] answers) {
    	int option = answers.length == 2 ? JOptionPane.YES_NO_OPTION : JOptionPane.YES_NO_CANCEL_OPTION;
		
    	int response = JOptionPane.showOptionDialog(this, question, question, option,
				JOptionPane.QUESTION_MESSAGE, null, answers, answers[0]);
		
		switch (response) {
		case JOptionPane.YES_OPTION:
			return (String) answers[0];
		case JOptionPane.NO_OPTION:
			return (String) answers[1];
		case JOptionPane.CANCEL_OPTION:
			return (String) answers[2];
		default:
			return "";
		}
    }

    public void showAnswerPanel(String answer) {
    	getContentPane().removeAll();
    	AnswerPanel answerPanel = new AnswerPanel(answer);
    	add(answerPanel, BorderLayout.CENTER);
    	pack();
    }

}
