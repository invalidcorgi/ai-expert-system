package com.sample;

import javax.swing.*;

import org.kie.api.runtime.KieSession;

import javax.imageio.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.*;
import java.io.*;

public class StartScreen extends JPanel
{

    public StartScreen(KieSession kSession, JFrame frame)
    {
        GridBagLayout gbl = new GridBagLayout();
        setLayout(gbl);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weighty = 0.1;
        gbc.gridwidth = GridBagConstraints.REMAINDER;

        JLabel title = new JLabel("Which director's movie should I watch?");
        title.setFont(new Font("DejaVu Serif", Font.BOLD, 32));
        gbl.setConstraints(title, gbc);
        add(title);
        
        JLabel author0 = new JLabel("Wojciech Regulski 132312");
        author0.setFont(new Font("DejaVu Serif", Font.BOLD, 18));
        gbl.setConstraints(author0, gbc);
        add(author0);

        JLabel author1 = new JLabel("Mikołaj Pudlicki 132311");
        author1.setFont(new Font("DejaVu Serif", Font.BOLD, 18));
        gbl.setConstraints(author1, gbc);
        add(author1);
        
        JButton button = new JButton("Start");
        gbl.setConstraints(button, gbc);
        add(button);
        
        button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.getContentPane().removeAll();
				frame.pack();
				kSession.fireAllRules();
			}
		});
        
    }
}

